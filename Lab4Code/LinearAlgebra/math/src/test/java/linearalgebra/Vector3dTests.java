package linearalgebra;
//NOAH DIPLARAKIS
import org.junit.Test;
import static org.junit.Assert.*;

public class Vector3dTests {
    
    @Test
    public void createVector3d_Test_GetMethods_Return_Good_Results(){
        Vector3d vector = new Vector3d(5, 6, 7);
        assertEquals(5,vector.getX(),0.001);
        assertEquals(6,vector.getY(),0.001);
        assertEquals(7,vector.getZ(),0.001);
    }
    @Test
    public void createVector3d_Test_MagnitudeMethod_Return_Expected_Result(){
        Vector3d vector = new Vector3d(5, 6, 7);
        assertEquals(10.4880884817,vector.magnitude(),0.0001);
    }
    @Test
    public void createTwoVector3ds_Test_dotProductMethod_Return_Expected_Result(){
        Vector3d vector1=new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        assertEquals(32,vector1.dotProduct(vector2),0.0001);
    }
    @Test
    public void createTwoVector3ds_Test_AddMethod_Returns_Expected_Result(){
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(4, 5, 6);
        Vector3d vectorAddResult = vector1.add(vector2);
        assertEquals(5,vectorAddResult.getX(),0.001);
        assertEquals(7,vectorAddResult.getY(),0.001);
        assertEquals(9,vectorAddResult.getZ(),0.001);
    }

}
